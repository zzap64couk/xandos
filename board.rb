# frozen_string_literal: true

# Contains functionality and data related to the game board.
class Board
  PLAYER_ONE = 'X'
  PLAYER_TWO = 'O'

  attr_reader :result, :player

  # Setup a blank board
  def initialize
    @board = [[' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' ']]
    # Can be 'draw', 'won'
    @game_result = ''
    @player = PLAYER_ONE
  end

  # Display the board in its current state
  def display_board
    puts
    @board.each do |row|
      row.each do |cell|
        print "|#{cell}"
      end
      puts '|'
      puts '-------'
    end
    puts
  end

  # Takes input from user
  def make_move
    row = input('row')
    column = input('column')

    [row, column]
  end

  # Get input for row
  def input(position)
    # Index in array is 0 so -1 from the input
    print "Enter the #{position}: "
    input = gets.strip.to_i - 1

    until input >= 0 && input <= 2
      print "#{position} must be between 1 and 3. Try again: "
      input = gets.strip.to_i - 1
    end
    input
  end

  # Add the move to the board
  def add_to_board(row, column)
    # Check if there is a place already there
    if @board[row][column] == ' '
      @board[row][column] = @player
      true
    else
      puts 'There already is a piece in that position. Try again.'
      false
    end
  end

  # Swap the player playing, i.e. the piece being used
  def swap_player
    @player = if @player == PLAYER_ONE
                PLAYER_TWO
              else
                PLAYER_ONE
              end
  end

  # Check if the game is over (won or drawn)
  def game_over?
    # Check board for 3 in a row
    @result = if win?(PLAYER_ONE)
                'win for player ' + PLAYER_ONE
              elsif win?(PLAYER_TWO)
                'win for player ' + PLAYER_TWO
              elsif @board.flatten.none? ' '
                'draw'
              end

    return true if @result

    false
  end

  def win?(piece)
    # Check rows, columns and diag (TODO: Can we do something like count elements in array (for rows?))
    if  ((@board[0][0] == piece && @board[0][1] == piece && @board[0][2] == piece) ||
        (@board[1][0] == piece && @board[1][1] == piece && @board[1][2] == piece) ||
        (@board[2][0] == piece && @board[2][1] == piece && @board[2][2] == piece)) ||
        # Check columns
        ((@board[0][0] == piece && @board[1][0] == piece && @board[2][0] == piece) ||
        (@board[0][1] == piece && @board[1][1] == piece && @board[2][1] == piece) ||
        (@board[0][2] == piece && @board[1][2] == piece && @board[2][2] == piece)) ||
        # Check diaganel
        ((@board[0][0] == piece && @board[1][1] == piece && @board[2][2] == piece) ||
        (@board[0][2] == piece && @board[1][1] == piece && @board[2][0] == piece))
      true
    end
  end
end
