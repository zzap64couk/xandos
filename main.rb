# frozen_string_literal: true

# X and O - I'm done with it Edition!
require 'byebug'
require './board.rb'

# Print out the instructions
puts 'Welcome to two player Os and Xs'
puts 'X goes first and then O etc.'
puts 'The winner is the person who gets 3 in a row'
puts 'Enter your move by enter the row and then column to mark your move'

game_board = Board.new

game_board.display_board

# Do the main game loop until somebody has won or it's a draw (i.e. board is full)
loop do
  puts "It's your go, player #{game_board.player}"
  row, column = game_board.make_move

  # Back to beginning if the piece can't be added to the board
  next unless game_board.add_to_board(row, column)

  game_board.display_board
  break if game_board.game_over?

  # Swap players
  game_board.swap_player
end

# To get here the game has ended
puts "The result of the game is a #{game_board.result}"
